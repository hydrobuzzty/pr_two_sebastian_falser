using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTWO
{
    public class StartGame : MonoBehaviour
    {
        /// <summary>
        /// Name of the scene to load
        /// </summary>
        [SerializeField] private string sceneName;

        /// <summary>
        /// Loads the Game Scene by name
        /// </summary>
        public void LoadGameScene()
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }
    }
}