using DG.Tweening;
using UnityEngine;

namespace PRTWO
{
    public class Boss : MonoBehaviour, IDamagable
    {
        /// <summary>
        /// Boss Health
        /// </summary>
        [SerializeField]
        private int health;

        /// <summary>
        /// Checks if boss is alive
        /// </summary>
        [SerializeField]
        private bool bossAlive = true;

        /// <summary>
        /// Determines the length of the shake
        /// </summary>
        [SerializeField]
        private float bossShakeDuration;

        /// <summary>
        /// Determines the vibration
        /// </summary>
        [SerializeField]
        private int bossShakeVibrato;

        /// <summary>
        /// Determines the power of the shake in X/Y/Z
        /// </summary>
        [SerializeField]
        private Vector3 bossShakeStrength;

        /// <summary>
        /// health Property
        /// </summary>
        public int Health
        {
            get 
            {
                return health;
            }
            set
            {
                health = value;
            }
        }

        /// <summary>
        /// bossAlive Property
        /// </summary>
        public bool BossAlive
        {
            get
            {
                 return bossAlive;
            }
            set
            {
                bossAlive = value;
            }
        }

        /// <summary>
        /// Interface Method, calculates damage on boss health with parameter
        /// </summary>
        /// <param name="damageAmount"></param>
        public void Damage(int damageAmount)
        {
            Health -= damageAmount;

            
            if (health <= 0)
            {
                KillBoss();

                return;
            }

            // Checking if the object is tweening, if it does, we complete the tween, so the object doesnt tween out
            if (DOTween.IsTweening(transform))
            {
                transform.DOComplete();
            }

            transform.DOShakePosition(bossShakeDuration, bossShakeStrength, bossShakeVibrato);
        }

        /// <summary>
        /// Playing a stronger shake tween, on completion we call the true killing method
        /// </summary>
        public void KillBoss()
        {
            if (DOTween.IsTweening(transform))
            {
                transform.DOComplete();
            }

            transform.DOShakePosition(bossShakeDuration * 2, bossShakeStrength * 3, bossShakeVibrato * 3)
                     .OnComplete(Finalise);
        }

        /// <summary>
        /// Setting bossAlive to false and destroying ONLY the SpriteRenderer, we still need the Boss-Gameobject/Component!
        /// </summary>
        public void Finalise()
        {
            BossAlive = false;
            Destroy(gameObject.GetComponent<SpriteRenderer>());
        }
    }
}